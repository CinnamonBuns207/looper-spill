﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public Rigidbody2D rb;
    public Animator animator;

    public float moveSpeed = 5f;
    private float moveLimiter = 0.75f;
    public bool disablePlayerMovement;
    
    Vector2 movement;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();

        StartCoroutine(MovementWait());

        animator.SetFloat("LastMoveX", PlayerPrefs.GetFloat("LastMoveX"));
        animator.SetFloat("LastMoveY", PlayerPrefs.GetFloat("LastMoveY"));
    }

    void Update()
    {
        if (!disablePlayerMovement)
        {
            movement.x = Input.GetAxisRaw("Horizontal");
            movement.y = Input.GetAxisRaw("Vertical");
        }

        animator.SetFloat("Horizontal", movement.x);
        animator.SetFloat("Vertical", movement.y);
        animator.SetFloat("Speed", movement.sqrMagnitude);
        
        if(movement.x == 1 || movement.x == -1 || movement.y == 1 || movement.y == -1)
        {
            animator.SetFloat("LastMoveX", movement.x);
            animator.SetFloat("LastMoveY", movement.y);
        }
    }

    void FixedUpdate()
    {
        if (movement.x != 0 && movement.y != 0)
        {
            movement.x *= moveLimiter;
            movement.y *= moveLimiter;
        }

        if (!disablePlayerMovement)
        {
            rb.MovePosition(rb.position + movement * moveSpeed * Time.fixedDeltaTime);
        }
    }

    IEnumerator MovementWait()
    {
        disablePlayerMovement = true;

        yield return new WaitForSeconds(0.75f);

        disablePlayerMovement = false;
    }
}