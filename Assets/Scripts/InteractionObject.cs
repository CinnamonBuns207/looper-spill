﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionObject : MonoBehaviour
{

    public bool inventory;              //If true this object can be stored in inventory
    public bool openable;               //If true this object can be opened
    public bool locked;                 //If true the object is locked
    public bool talks;                  //if this is true, object can talk

    public GameObject itemNeeded;       //Item needed to interact with this item
    public string message;              //the message the object will give

    public Animator anim;

    public void DoInteraction()
    {
        //Picked up an put in inventory
        gameObject.SetActive(false);
    }

    public void Open()
    {
        anim.SetBool("open", true);
    }

    public void Talk()
    {
        Debug.Log(message);
    }
}
