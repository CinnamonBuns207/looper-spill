﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    public GameObject player;
    public Transform playerTrans;
    public Animator transitionAnim;

    public string sceneToLoad;
    public float transitionTime = 1f;

    string currentScene;
    string prevScene;

    bool gameQuit;

    public Vector3 gameStartPos;
    public Vector3 scene1To2Pos;
    public Vector3 scene2To1Pos;

    public Vector3 scene2ToCabinPos;
    public Vector3 sceneCabinTo2Pos;

    public Vector3 scene3To2Pos;
    public Vector3 scene2To3Pos;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerTrans = player.GetComponent<Transform>();

        transitionAnim = GetComponentInChildren<Animator>();

        gameQuit = false;

        currentScene = SceneManager.GetActiveScene().name;
        prevScene = PlayerPrefs.GetString("prevScene", "No scene");

        // SceneSwapPlayerPos(currentScene, prevScene, sceneStartPos)
        SceneSwapPlayerPos("Gussi1", "No scene", gameStartPos);
        
        SceneSwapPlayerPos("Gussi1", "Gussi2", scene2To1Pos);
        SceneSwapPlayerPos("Gussi2", "Gussi1", scene1To2Pos);

        SceneSwapPlayerPos("Gussi2", "GussiCabin", sceneCabinTo2Pos);

        SceneSwapPlayerPos("Gussi2", "Gussi3", scene3To2Pos);
        SceneSwapPlayerPos("Gussi3", "Gussi2", scene2To3Pos);
    }

    void Update()
    {
        if (Input.GetKeyDown("escape"))
        {
            ReloadGame();   
        }
    }

    void OnDestroy()
    {
        if (!gameQuit)
        {
            PlayerPrefs.SetString("currentScene", sceneToLoad);
            PlayerPrefs.SetString("prevScene", currentScene);
        }
        else
        {
            PlayerPrefs.DeleteKey("currentScene");
            PlayerPrefs.DeleteKey("prevScene"); 
        }
    }

    void OnApplicationQuit()
    {
        gameQuit = true;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Player") && !other.isTrigger)
        {
            LoadNextScene();
        }
    }

    void ReloadGame()
    {
        prevScene = PlayerPrefs.GetString("prevScene", "No scene");
        currentScene = "Gussi1";
        SceneManager.LoadScene("Gussi1");
    }

    public void LoadNextScene()
    {
        player.GetComponent<PlayerMovement>().disablePlayerMovement = true;
        player.GetComponent<Animator>().SetBool("Transitioning", true);

        PlayerPrefs.SetFloat("LastMoveX", player.GetComponent<Animator>().GetFloat("LastMoveX"));
        PlayerPrefs.SetFloat("LastMoveY", player.GetComponent<Animator>().GetFloat("LastMoveY"));

        StartCoroutine(SceneTransition(sceneToLoad));
    }

    IEnumerator SceneTransition(string sceneName)
    {
        transitionAnim.SetTrigger("Start");

        yield return new WaitForSeconds(transitionTime);

        SceneManager.LoadScene(sceneName);
    }

    void SceneSwapPlayerPos(string curSce, string preSce, Vector3 pos)
    {
        if(currentScene == curSce && prevScene == preSce)
        {
            playerTrans.position = pos;
        }
    }
}
