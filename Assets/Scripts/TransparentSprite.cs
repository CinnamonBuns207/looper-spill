﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransparentSprite : MonoBehaviour
{
    public float transparent = 0.5f;
    float solidColor = 1f;

    void OnTriggerEnter2D(Collider2D player)
    {
        if(player.tag == "Player" && !player.isTrigger)
        {
            GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, transparent);
        }
    }

    void OnTriggerExit2D(Collider2D player)
    {
        if(player.tag == "Player" && !player.isTrigger)
        {
            GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, solidColor);
        }
    }
}