﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SortingOrderSprites : MonoBehaviour
{
    public Transform playerTransform;
    public SpriteRenderer sprite;

    public string playerInFront = "Sprites_Behind_Player";
    public string playerBehind = "Sprites_Frontof_Player";

    void Start()
    {
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        sprite = GetComponentInParent<SpriteRenderer>();

        SortingOrder();
    }
    void Update()
    {
        SortingOrder();
    }

    void SortingOrder()
    {
        // Denne if/else skal sjekke om spillerens sko er over eller under y-verdien av objectet
        // Lavere y-verdi vil si foran objectet og derfor er player-spriten foran objectets

        if (transform.position.y < playerTransform.position.y)
        {
            sprite.sortingLayerName = playerBehind;
            sprite.sortingOrder = -Mathf.RoundToInt(transform.position.y);
        }
        else if (transform.position.y > playerTransform.position.y)
        {
            sprite.sortingLayerName = playerInFront;
            sprite.sortingOrder = -Mathf.RoundToInt(transform.position.y);
        }
    }
}
