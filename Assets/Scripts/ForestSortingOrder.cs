﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForestSortingOrder : MonoBehaviour
{
    public SpriteRenderer sprite;

    void Start()
    {
        sprite = GetComponent<SpriteRenderer>();

        sprite.sortingOrder = -Mathf.RoundToInt(transform.position.y * 10);
    }
}
