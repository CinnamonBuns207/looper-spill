﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteraction : MonoBehaviour
{

    public GameObject currentInterObj = null;
    public InteractionObject currentInteractionObjScript = null;
    //public Inventory inventory;

     // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown ("Interact") && currentInterObj)
        {
           /* if (currentInteractionObjScript.inventory)
            {
                inventory.AddItem(currentInterObj);
            }*/

            if (currentInteractionObjScript.openable)
            {
                if (currentInteractionObjScript.locked)
                {
                   /* if (inventory.FindItem (currentInteractionObjScript.itemNeeded))
                    {
                        currentInteractionObjScript.locked = false;
                        Debug.Log(currentInterObj.name + "was unlocked");
                    }
                    else
                    {
                        Debug.Log(currentInterObj.name + "was not unlocked");
                    }*/
                }
                else
                {
                    Debug.Log(currentInterObj.name + " is unlocked");
                    currentInteractionObjScript.Open();
                }
            }

            if (currentInteractionObjScript.talks)
            {
                currentInteractionObjScript.Talk();
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("interObject"))
        {
            Debug.Log(other.name);
            currentInterObj = other.gameObject;
            currentInteractionObjScript = currentInterObj.GetComponent<InteractionObject>();
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("interObject"))
        {
            if(other.gameObject == currentInterObj)
            {
                currentInterObj = null;
            }
        }
    }
}
